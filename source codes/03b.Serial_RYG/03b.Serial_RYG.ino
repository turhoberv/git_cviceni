int red = 16;
int yellow = 17;
int green = 13;
int cislo = 0;
int stovky = 0, desitky = 0, jednotky = 0;
void setup() {
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
  Serial.begin(9600);
  Serial.println("----------------------");
}
void loop() {
  while(Serial.available()){
    Serial.println("cislo = "+Serial.parseInt());
    digitalWrite(red, (stovky = (cislo/100)%10)HIGH:LOW);
    digitalWrite(yellow, (desitky = (cislo/10)%10)?HIGH:LOW);
    digitalWrite(green, (jednotky = cislo%10)?HIGH:LOW);
    delay(10);
  }
}
